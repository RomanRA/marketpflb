
import ru.shop.market.Market;
import ru.shop.products.books.CookingBook;
import ru.shop.products.Products;
import ru.shop.products.books.IsotericaBook;
import ru.shop.products.books.ProgrammingBook;
import ru.shop.products.discs.CdDvdDiscs;

import static lib.holder.Holder.holder;
import static lib.holder.Holder.products;
import static lib.holder.KeysMap.*;

public class InitHolder {
    public void initialization() {
        Market market = new Market();

        Products programmingBook = new ProgrammingBook("LearnJava", 1568798,
                1500, 900, "Java");

        Products cookBook = new CookingBook("TeastyMils ", 1564898,
                950, 350, "Cocaine");

        Products isoterBook = new IsotericaBook("BlackWidow", 78464,
                660, 358, 18);

        Products dvd = new CdDvdDiscs("Windows 3.11", 123456,700,"Programm");
        Products cd = new CdDvdDiscs("Ac-Dc", 123456,700,"Music");
        Products blueRAy = new CdDvdDiscs("Avengers", 123456,700,"Film");


        holder.put(ManyMoney, market);
        products.put(ProgrammingBook, programmingBook);
        products.put(CookingBook, cookBook);
        products.put(Isoterica, isoterBook);
        products.put(CDs, cd);
        products.put(DVDs, dvd);
        products.put(BlueRay, blueRAy);
    }
}
