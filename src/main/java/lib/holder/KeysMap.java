package lib.holder;

public enum KeysMap {
    ManyMoney,
    CDs,
    DVDs,
    BlueRay,
    ProgrammingBook,
    CookingBook,
    Isoterica
}
