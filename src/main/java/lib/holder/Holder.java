package lib.holder;

import ru.shop.market.Market;
import ru.shop.products.Products;

import java.util.HashMap;
import java.util.Map;

public class Holder {

    public static Map<KeysMap, Market> holder = new HashMap<KeysMap, Market>();
    public static Map<KeysMap, Products> products = new HashMap<KeysMap, Products>();

}
