package ru.shop.products;

public class Books extends Products {
    public int getCountSheets() {
        return countSheets;
    }

    public void setCountSheets(int countSheets) {
        this.countSheets = countSheets;
    }

    protected int countSheets;

    public Books(String productName, long barCode, float priceProd, int countSheets) {
        super(productName, barCode, priceProd);
        this.countSheets = countSheets;
    }

    @Override
    public String toString() {
        return "Books{" +
                "countSheets=" + countSheets +
                ", bookName='" + productName + '\'' +
                ", barCode=" + barCode +
                ", priceProd=" + priceProd +
                '}';
    }
}
