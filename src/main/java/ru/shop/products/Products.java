package ru.shop.products;


public class Products {
    protected String productName;

    public String getProductName() {
        return productName;
    }

    public void setBookName(String bookName) {
        this.productName = bookName;
    }

    public long getBarCode() {
        return barCode;
    }

    public void setBarCode(long barCode) {
        this.barCode = barCode;
    }

    public float getPriceProd() {
        return priceProd;
    }

    public void setPriceProd(float priceProd) {
        this.priceProd = priceProd;
    }

    protected long barCode;
    protected float priceProd;

    public Products(String productName, long barCode, float priceProd) {
        this.productName = productName;
        this.barCode = barCode;
        this.priceProd = priceProd;
    }


    @Override
    public String toString() {
        return "Products{" +
                "bookName='" + productName + '\'' +
                ", barCode=" + barCode +
                ", priceProd=" + priceProd +
                '}';
    }
}
