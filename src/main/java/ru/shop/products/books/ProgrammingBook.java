package ru.shop.products.books;

import ru.shop.products.Books;

public class ProgrammingBook extends Books {

    @Override
    public String toString() {
        return "ProgrammingBook{" +
                "nameLang='" + nameLang + '\'' +
                ", countSheets=" + countSheets +
                ", bookName='" + productName + '\'' +
                ", barCode='" + barCode + '\'' +
                ", priceProd=" + priceProd +
                '}';
    }

    public ProgrammingBook(String productName,  long barCode, float priceProd, int countSheets, String nameLang) {
        super(productName,  barCode, priceProd, countSheets);
        this.nameLang = nameLang;
    }

    public String getNameLang() {
        return nameLang;
    }

    public void setNameLang(String nameLang) {
        this.nameLang = nameLang;
    }

    private String nameLang;


}
