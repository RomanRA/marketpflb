package ru.shop.products.books;

import ru.shop.products.Books;

public class CookingBook extends Books {
    @Override
    public String toString() {
        return "CookingBook{" +
                "mainIngridient='" + mainIngridient + '\'' +
                ", countSheets=" + countSheets +
                ", bookName='" + productName + '\'' +
                ", barCode=" + barCode +
                ", priceProd=" + priceProd +
                '}';
    }

    public String getMainIngridient() {
        return mainIngridient;
    }

    public void setMainIngridient(String mainIngridient) {
        this.mainIngridient = mainIngridient;
    }

    public CookingBook(String productName, long barCode, float priceProd, int countSheets, String mainIngridient) {
        super(productName, barCode, priceProd, countSheets);
        this.mainIngridient = mainIngridient;
    }

    private String mainIngridient;
}
