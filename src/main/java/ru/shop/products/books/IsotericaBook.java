package ru.shop.products.books;

import ru.shop.products.Books;

public class IsotericaBook extends Books {
    public int getHumanAge() {
        return humanAge;
    }

    public void setHumanAge(int humanAge) {
        this.humanAge = humanAge;
    }

    private int humanAge;

    public IsotericaBook(String productName, long barCode, float priceProd, int countSheets, int humanAge) {
        super(productName, barCode, priceProd, countSheets);
        this.humanAge = humanAge;
    }

    @Override
    public String toString() {
        return "IsotericaBook{" +
                "humanAge=" + humanAge +
                ", countSheets=" + countSheets +
                ", bookName='" + productName + '\'' +
                ", barCode=" + barCode +
                ", priceProd=" + priceProd +
                '}';
    }
}
