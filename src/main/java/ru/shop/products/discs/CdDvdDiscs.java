package ru.shop.products.discs;

import ru.shop.products.Products;

public class CdDvdDiscs extends Products{
    private String whatInside;

    public CdDvdDiscs(String productName, long barCode, float priceProd, String whatInside) {
        super(productName, barCode, priceProd);
        this.whatInside = whatInside;
    }

    public String getWhatInside() {
        return whatInside;
    }

    public void setWhatInside(String whatInside) {
        this.whatInside = whatInside;
    }

    @Override
    public String toString() {
        return "CdDvdDiscs{" +
                "whatInside='" + whatInside + '\'' +
                ", cdName='" + productName + '\'' +
                ", barCode=" + barCode +
                ", priceProd=" + priceProd +
                '}';
    }
}
