package ru.shop.market;

import java.util.LinkedList;


public class Market extends MyMoney{
    private LinkedList<String> getProductName() {
        return productName;
    }


    private LinkedList<String> productName = new LinkedList<String>();

    public void inventarization() {
        System.out.println("Инвентаризация начата!\nИмеется такой товар:");
        for (String prodName : getProductName()) {
            System.out.println(prodName);
        }
        myMoney();
    }

    public void addProduct(String addNameProduct, float price) {
        getProductName().addLast(addNameProduct);
        buyProduct(price);
    }

    public void sellProduct(String sellNameProduct, float sellPrice) {
        if(getProductName().contains(sellNameProduct)){
            getProductName().remove(sellNameProduct);
            sellProduct(sellPrice);
        }else{
            System.out.println("Удалить вот этот -> " + sellNameProduct
                    + " <- элемент не удалось!\n Нет такого элемента!\n\nЕсть такие:");
            for (String prodName : productName) {
                System.out.println(prodName);
            }
        }
    }

}
