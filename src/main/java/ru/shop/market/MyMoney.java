package ru.shop.market;

public class MyMoney {

    private void setMyCashe(float myCashe) {
        this.myCashe = myCashe;
    }

    private float getMyCashe() {
        return myCashe;
    }

    private float myCashe = 15000;



    public void myMoney() {
        System.out.println("У меня есть - " + getMyCashe() + " руб.");
    }

    public void sellProduct(float giveMeMyMoney) {
        myCashe += giveMeMyMoney;
        setMyCashe(myCashe);
    }

    public void buyProduct(float chokeOnYourMoney) {
        float ohhNo = myCashe - chokeOnYourMoney;
        if (ohhNo < 0) {
            System.out.println("Нужно больще золота...");
            System.out.println("Больше не могу купить товара!");
        } else {
            myCashe -= chokeOnYourMoney;
            setMyCashe(myCashe);
        }
    }


}
