

import ru.shop.market.Market;
import ru.shop.products.Products;
import ru.shop.products.books.CookingBook;
import ru.shop.products.books.IsotericaBook;
import ru.shop.products.books.ProgrammingBook;
import ru.shop.products.discs.CdDvdDiscs;

import static lib.holder.Holder.*;
import static lib.holder.KeysMap.*;

public class Main {

    public static void main(String[] args) {
        Market market = new Market();
//        InitHolder initHolder = new InitHolder();
//        initHolder.initialization();
        initialization();

//        добавил продуктов, по одному, каждого типа
        market.addProduct(products.get(ProgrammingBook).getProductName(),products.get(ProgrammingBook).getPriceProd());
        market.addProduct(products.get(CookingBook).getProductName(),products.get(CookingBook).getPriceProd());
        market.addProduct(products.get(Isoterica).getProductName(),products.get(Isoterica).getPriceProd());
        market.addProduct(products.get(CDs).getProductName(),products.get(CDs).getPriceProd());
        market.addProduct(products.get(DVDs).getProductName(),products.get(DVDs).getPriceProd());
        market.addProduct(products.get(BlueRay).getProductName(),products.get(BlueRay).getPriceProd());
//        смотрим что есть на складе
        market.inventarization();
        System.out.println();
        // продаем два товара за произвольную цену
        market.sellProduct(products.get(ProgrammingBook).getProductName(), 1700);
        market.sellProduct(products.get(DVDs).getProductName(), 4500);
        market.inventarization();
        System.out.println("====================================================================");
        System.out.println();
//        Просматриваем классы с товарами
        System.out.println(products.get(DVDs));
        System.out.println(products.get(ProgrammingBook));
        System.out.println(products.get(BlueRay));
        System.out.println(products.get(CDs));
        System.out.println(products.get(Isoterica));
        System.out.println(products.get(CookingBook));

    }

    private static void initialization() {

        holder.put(ManyMoney, new Market());

        products.put(ProgrammingBook, new ProgrammingBook("LearnJava", 1568798,
                1500, 900, "Java"));
        products.put(CookingBook, new CookingBook("TeastyMils ", 1564898,
                950, 350, "Cocaine"));
        products.put(Isoterica, new IsotericaBook("BlackWidow", 78464,
                660, 358, 18));
        products.put(CDs,  new CdDvdDiscs("Windows 3.11", 123456,3700,"Programm"));
        products.put(DVDs, new CdDvdDiscs("Ac-Dc", 123456,1700,"Music"));
        products.put(BlueRay,  new CdDvdDiscs("Avengers", 123456,700,"Film"));
    }
}
