import org.junit.Test;

import java.util.LinkedList;

public class Tester {
    public static void main(String[] args) {
        String[] animals = {"Хаски", "Морж"};// массив строк 1
        String[] food = {"колбаски", "корж"}; // массив строк 2

//составляем строки из элементов массивов и связующего слова
        String result1 = animals[0].concat(" ест ").concat(food[0]);
        String result2 = animals[1].concat(" ест ").concat(food[1]);

//выводим на консоль
        System.out.println(result1);
        System.out.println(result2);
    }

    @Test
    public void sddsdsd() {
        LinkedList<String> states = new LinkedList<String>();

        // добавим в список ряд элементов
        states.add("Germany, 9999");
        states.add("France");
        states.addLast("Great Britain"); // добавляем на последнее место
        states.addFirst("Spain"); // добавляем на первое место
        states.add(1, "Italy"); // добавляем элемент по индексу 1

        System.out.printf("List has %d elements \n", states.size());
        System.out.println("states.get(1) - " + states.get(1));
        states.set(1, "Portugal");


        for (int i = 0; i < states.size(); i++) {
            System.out.println("state " + i + " " + states.get(i));
        }


        // проверка на наличие элемента в списке
        if (states.contains("Germany")) {

            System.out.println("List contains Germany");
        } else {
            System.out.println("No item");
        }

        states.remove("Germany");
        states.removeFirst(); // удаление первого элемента
        states.removeLast(); // удаление последнего элемента
        for (int i = 0; i < states.size(); i++) {
            System.out.println("state " + i + " " + states.get(i));
        }
    }


}
